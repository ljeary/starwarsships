﻿namespace StarWarsCore.ValueObjects
{
    using System;

    public class Distance
    {
        public DistanceType Description { get; set; }

        public int Value { get; set; }

        public Distance(DistanceType description, int value)
        {
            this.Description = description;
            this.Value = value;
        }

        public double DistanceInMegaLightYears()
        {
            switch (this.Description)
            {
                case DistanceType.MGLT:
                    return this.Value;
                
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

    }
}

﻿namespace StarWarsCore.ValueObjects
{
    using StarWarsCore.Enums;
    using System;

    public class LengthOfTime
    {
        public int Number { get; set; }

        public LengthOfTimeType LengthOfTimeType { get; set; }

        public LengthOfTime(int number, LengthOfTimeType lengthOfTimeType)
        {
            this.Number = number;
            this.LengthOfTimeType = lengthOfTimeType;
        }

        public int ToHours()
        {
            int hours = 0;
            switch (this.LengthOfTimeType)
            {
                case LengthOfTimeType.Year:
                    hours = this.Number * 365 * 24;
                    break;
                case LengthOfTimeType.Day:
                    hours = this.Number * 24;
                    break;
                case LengthOfTimeType.Hour:
                    hours = this.Number;
                    break;
                case LengthOfTimeType.Week:
                    hours = this.Number * 7 * 24;
                    break;
                case LengthOfTimeType.Month:
                    hours = ((365 * 24) / 12) * this.Number;
                    break;
                default:
                    throw new FormatException();

            }
            return hours;
        }
    }
}

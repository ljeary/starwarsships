﻿namespace StarWarsCore.Models
{
    using StarWarsCore.ValueObjects;

    public interface IStarShip
    {
        string Name { get; set; }

        LengthOfTime? ResupplyLimit { get; set; }

        int? MegaLightYearsPerHour { get; set; }

    }
}

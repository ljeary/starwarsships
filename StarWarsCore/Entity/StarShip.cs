﻿namespace StarWarsCore.Models
{
    using StarWarsCore.ValueObjects;

    public class StarShip : IStarShip
    {
        public string Name { get; set; }

        public LengthOfTime? ResupplyLimit { get; set; }

        public int? MegaLightYearsPerHour { get; set; }

    }
}

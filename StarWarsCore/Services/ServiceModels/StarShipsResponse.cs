﻿namespace StarWarsCore.Models
{
    using System.Collections.Generic;

    public class StarShipsResponse
    {
        public int Count { get; set; }

        public string Next { get; set; }

        public IList<StarShipResponse> Results { get; set; }

        
    }
}

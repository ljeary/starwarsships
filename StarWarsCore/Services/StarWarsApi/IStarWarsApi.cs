﻿namespace StarWarsCore.Services
{
    using StarWarsCore.Models;
    using System.Threading.Tasks;

    public interface IStarWarsApi
    {
        Task<StarShipsResponse> GetShips(int page);
    
    }
}

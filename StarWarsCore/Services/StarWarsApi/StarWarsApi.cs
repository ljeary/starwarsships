﻿namespace StarWarsCore.Services
{
    using StarWarsCore.Models;
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;

    public class StarWarsApi : IStarWarsApi
    {
        private string baseUrl = "https://swapi.co/api";

        private HttpClient client = new HttpClient();

        public async Task<StarShipsResponse> GetShips(int page)
        {
            try
            {
                var result = await client.GetAsync($"{baseUrl}/starships/?page={page}");
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var jsonStarShipResponse = await result.Content.ReadAsStringAsync();
                    var starShipsResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<StarShipsResponse>(jsonStarShipResponse);
                    return starShipsResponse;
                } else if (result.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    return new StarShipsResponse();
                } else
                {
                    throw new NotSupportedException();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [Obsolete] // This triggered their rate limiting so removed but would use and throttle if required to prevent getting each page synchronously 
        private async Task GetShipsInParrallel(List<StarShipResponse> starShips, StarShipsResponse initial)
        {
            BlockingCollection<StarShipResponse> listOfShips = new BlockingCollection<StarShipResponse>();

            int lastPage = (int)Math.Ceiling((decimal)initial.Count / initial.Results.Count);
            var tasks = new List<Task<StarShipsResponse>>();

            for (int index = 0; index < lastPage; index++)
            {
                tasks.Add(GetShips(index + 1));
            }

            await Task.WhenAll(tasks);

            foreach (var getShipTask in tasks)
            {
                var taskResult = getShipTask.Result;
                if (taskResult.Results?.Count > 0)
                {
                    foreach (var ship in taskResult.Results)
                    {
                        listOfShips.Add(ship);
                    }
                }
            }

            starShips.AddRange(listOfShips.ToArray());
        }
    }
}

﻿namespace StarWarsCore.Enums
{
    public enum LengthOfTimeType
    {
        Hour,
        Day,
        Week,
        Month,
        Year        
    }
}

﻿namespace StarWarsCore
{
    using System.ComponentModel;

    public enum DistanceType
    {
        [Description("Mega Light Years")]
        MGLT,

        [Description("Light Year")]
        LY

    }
}

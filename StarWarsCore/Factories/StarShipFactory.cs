﻿namespace StarWarsCore.Factories
{
    using StarWarsCore.Models;
    using System.Collections.Generic;

    public static class StarShipsFactory
    {
        /// <summary>
        /// create a star ship from the the star ship api response
        /// </summary>
        /// <param name="starShipResponse"></param>
        /// <returns></returns>
        public static IStarShip Create(StarShipResponse starShipResponse)
        {
            int speedInMegaLightYears;
            bool isValidSpeed = int.TryParse(starShipResponse.MGLT, out speedInMegaLightYears);
            
            return new StarShip()
            {
                Name = starShipResponse.Name,
                MegaLightYearsPerHour = isValidSpeed ? (int?)speedInMegaLightYears : null,
                ResupplyLimit = LengthOfTimeFactory.Create(starShipResponse.Consumables),
            };
        }

        /// <summary>
        /// create a list of star ships from the list of star ship responses from the api
        /// </summary>
        /// <param name="starShipResponses"></param>
        /// <returns></returns>
        public static List<IStarShip> Create(IList<StarShipResponse> starShipResponses)
        {
            List<IStarShip> starShips = new List<IStarShip>();
            foreach (var starShipResponse in starShipResponses)
            {
                starShips.Add(Create(starShipResponse));
            }
            return starShips;
        }
    }
}

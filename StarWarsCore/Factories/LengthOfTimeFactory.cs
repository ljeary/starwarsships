﻿namespace StarWarsCore.Factories
{
    using StarWarsCore.Enums;
    using StarWarsCore.ValueObjects;
    using System;
    using System.Text.RegularExpressions;

    public static class LengthOfTimeFactory
    {
        public static LengthOfTime? Create(string lengthOfTimeDescription)
        {
            if (lengthOfTimeDescription.ToLower().Contains("unknown"))
                return null;

            var matches = Regex.Match(lengthOfTimeDescription, "(?<number>[0-9]*)\\s(?<time>.*)");
            var number = Convert.ToInt32(matches.Groups["number"].Value);
            var time = matches.Groups["time"].Value;
            LengthOfTimeType lengthOfTimeType;

            switch (time.ToLower())
            {
                case "year":
                case "years":
                    lengthOfTimeType = LengthOfTimeType.Year;
                    break;
                case "day":
                case "days":
                    lengthOfTimeType = LengthOfTimeType.Day;
                    break;
                case "hour":
                case "hours":
                    lengthOfTimeType = LengthOfTimeType.Hour;
                    break;
                case "week":
                case "weeks":
                    lengthOfTimeType = LengthOfTimeType.Week;
                    break;
                case "month":
                case "months":
                    lengthOfTimeType = LengthOfTimeType.Month;
                    break;
                default:
                    throw new FormatException();
            }
            return new LengthOfTime(number, lengthOfTimeType);
        }
    }
}

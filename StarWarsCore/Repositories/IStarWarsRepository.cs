﻿namespace StarWarsCore.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using StarWarsCore.Models;

    public interface IStarWarsRepository
    {
        Task<IList<StarShipResponse>> GetAllShips();

    }
}
﻿namespace StarWarsCore.Repositories
{
    using StarWarsCore.Models;
    using StarWarsCore.Services;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class StarWarsRepository : IStarWarsRepository
    {
        private IStarWarsApi starWarsService;
        public StarWarsRepository(IStarWarsApi starWarsService)
        {
            this.starWarsService = starWarsService;
        }

        public async Task<IList<StarShipResponse>> GetAllShips()
        {
            try
            {
                List<StarShipResponse> starShips = new List<StarShipResponse>();

                var initial = await this.starWarsService.GetShips(1);

                if (initial.Count > 0 && initial.Results != null)
                {
                    starShips.AddRange(initial.Results);
                    if (initial.Next != null)
                    {
                        int lastPage = (int)Math.Ceiling((decimal)initial.Count / initial.Results.Count);
                        for (int index = 2; index < lastPage + 1; index++)
                        {
                            var ships = await this.starWarsService.GetShips(index);
                            if (ships.Results?.Count > 0)
                            {
                                starShips.AddRange(ships.Results);
                            }
                        }
                    }
                }
                return starShips;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}

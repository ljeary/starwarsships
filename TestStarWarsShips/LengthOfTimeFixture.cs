﻿namespace TestStarWarsShips
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using StarWarsCore.Enums;
    using StarWarsCore.ValueObjects;

    [TestClass]
    public class LengthOfTimeFixture
    {

        [TestMethod]
        public void TimeCalculations()
        {
            LengthOfTime lot = new LengthOfTime(2, LengthOfTimeType.Day);
            Assert.IsTrue(lot.ToHours() == 48);

            lot = new LengthOfTime(2, LengthOfTimeType.Month);
            Assert.IsTrue(lot.ToHours() == 1460);

            lot = new LengthOfTime(2, LengthOfTimeType.Week);
            Assert.IsTrue(lot.ToHours() == 24 * 7 * 2);

            lot = new LengthOfTime(2, LengthOfTimeType.Year);
            Assert.IsTrue(lot.ToHours() == 24 * 365 * 2);
        }
    }
}

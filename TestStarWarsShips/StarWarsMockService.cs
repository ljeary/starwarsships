﻿namespace TestStarWarsShips
{
    using StarWarsCore.Models;
    using StarWarsCore.Services;
    using System.IO;
    using System.Threading.Tasks;

    public class StarWarsMockService : IStarWarsApi
    {
        public Task<StarShipsResponse> GetShips(int page)
        {
            string file = File.ReadAllText($"./TestData/StarShipPage{page}.json");
            return Task.FromResult<StarShipsResponse>(Newtonsoft.Json.JsonConvert.DeserializeObject<StarShipsResponse>(file));
        }
    }
}

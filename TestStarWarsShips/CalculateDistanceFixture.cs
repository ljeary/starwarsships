namespace TestStarWarsShips
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using StarWarsCore;
    using StarWarsCore.Repositories;
    using StarWarsCore.ValueObjects;
    using StarWarsDomain.Service;
    using System.Linq;

    [TestClass]
    public class CalculateDistanceFixture
    {

        [TestMethod]
        public void GetStarShipFuelStops()
        {
            StarWarsMockService starWarsMockService = new StarWarsMockService();
            StarWarsRepository starWarsRepository = new StarWarsRepository(starWarsMockService);
            SpaceTravelService s = new SpaceTravelService(starWarsRepository);

            Distance distanceBetweenPlanets = new Distance(DistanceType.MGLT, 100001);
            var t = s.GetStopsRequiredForShips(distanceBetweenPlanets);
            t.Wait();

            Assert.IsTrue(t.Result.Count > 0);
            Assert.IsTrue(t.Result.Count(x => x.ToString().Contains("Unknown")) > 0);

            Assert.IsTrue(t.Result.Count(x => x.ToString().Contains("Unknown")) > 0);
        }

    }
}

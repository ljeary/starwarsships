namespace TestStarWarsShips
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using StarWarsCore.Factories;
    using StarWarsCore.Models;
    using StarWarsCore.Repositories;
    using StarWarsCore.Services;
    using System.Collections.Generic;

    [TestClass]
    public class StarWarsServiceFixture
    {
        [TestMethod]
        public void GetShipsFirstPage()
        {
            IStarWarsApi service = new StarWarsMockService();
            var ships = service.GetShips(1);
            ships.Wait();
            Assert.IsTrue(ships.Result.Count > 0);
        }

        [TestMethod]
        public void GetAllShipResponses()
        {
            IStarWarsApi service = new StarWarsMockService();
            StarWarsRepository repository = new StarWarsRepository(service);

            var ships = repository.GetAllShips();
            ships.Wait();
            Assert.IsTrue(ships.Result.Count == 19);
        }

        [TestMethod]
        public void GetAllShips()
        {
            IStarWarsApi service = new StarWarsMockService();
            StarWarsRepository repository = new StarWarsRepository(service);

            var shipResponseTask = repository.GetAllShips();
            shipResponseTask.Wait();
            List<IStarShip> ships = StarShipsFactory.Create(shipResponseTask.Result);
            Assert.IsTrue(ships.Count == 19);
        }


        [TestMethod]
        public void GetAllShipsIntegrationTest()
        {
            IStarWarsApi service = new StarWarsApi();
            StarWarsRepository repository = new StarWarsRepository(service);

            var ships = repository.GetAllShips();
            ships.Wait();
            Assert.IsTrue(ships.Result.Count == 37);
        }

    }
}

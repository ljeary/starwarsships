﻿namespace StarWarsDomain.Model
{
    public class StarShipResupplyStops
    {
        public string Name { get; set; }

        public int? StopsRequired { get; set; }

        public override string ToString()
        {
            return $"{Name}: {(StopsRequired == null ? "Unknown" : StopsRequired.ToString())}";
        }
    }
}
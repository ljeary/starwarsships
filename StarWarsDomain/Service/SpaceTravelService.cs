﻿namespace StarWarsDomain.Service
{
    using StarWarsCore.Factories;
    using StarWarsCore.Models;
    using StarWarsCore.Repositories;
    using StarWarsCore.ValueObjects;
    using StarWarsDomain.Model;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class SpaceTravelService : ISpaceTravelService
    {
        private IStarWarsRepository starWarsRepository;

        public SpaceTravelService(IStarWarsRepository starWarsRepository)
        {
            this.starWarsRepository = starWarsRepository;
        }

        /// <summary>
        /// This method will get all the starships from the repository and calculate 
        /// the number of stops required to get the ship to the destination
        /// </summary>
        /// <param name="distance">the distance the ship has to travel</param>
        /// <returns></returns>
        public async Task<IList<StarShipResupplyStops>> GetStopsRequiredForShips(Distance distance)
        {
            IList<StarShipResupplyStops> starShipFuelStops = new List<StarShipResupplyStops>();
            var shipsResponses = await starWarsRepository.GetAllShips();
            var ships = StarShipsFactory.Create(shipsResponses);

            foreach (var ship in ships)
            {
                StarShipResupplyStops shipFuelStop = new StarShipResupplyStops();
                shipFuelStop.Name = ship.Name;
                shipFuelStop.StopsRequired = GetStopsRequiredToTravelDistance(distance, ship);
                starShipFuelStops.Add(shipFuelStop);
            }

            return starShipFuelStops;
        }

        /// <summary>
        /// Calculates the required resupply stops between planets.
        /// 
        /// <para>
        /// Assumes that the ship has a full supply when leaving the planet
        /// </para>
        /// </summary>
        /// <param name="distance">the distance between the 2 planets</param>
        /// <param name="ship">the ship that they are travelling in</param>
        /// <returns></returns>
        private int? GetStopsRequiredToTravelDistance(Distance distance, IStarShip ship)
        {
            // 100 / 20 = 5
            // 20 already
            // so i need 4 more stops

            if (ship.MegaLightYearsPerHour == null || ship.ResupplyLimit == null)
                return null;

            double hoursOfTravelRequired = distance.DistanceInMegaLightYears() / ship.MegaLightYearsPerHour.Value;
            int resupplyStopsRequired = (int)Math.Floor(hoursOfTravelRequired / ship.ResupplyLimit.ToHours());
            return resupplyStopsRequired;
        }

    }
}

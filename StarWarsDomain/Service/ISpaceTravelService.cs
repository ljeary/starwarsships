﻿namespace StarWarsDomain.Service
{
    using StarWarsCore.ValueObjects;
    using StarWarsDomain.Model;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface ISpaceTravelService
    {
        Task<IList<StarShipResupplyStops>> GetStopsRequiredForShips(Distance distance);
    }
}
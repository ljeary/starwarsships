﻿using Microsoft.Extensions.DependencyInjection;
using StarWarsCore;
using StarWarsCore.Models;
using StarWarsCore.Repositories;
using StarWarsCore.Services;
using StarWarsCore.ValueObjects;
using StarWarsDomain.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StartWarsShips
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var services = new ServiceCollection()
            .AddSingleton<IStarWarsApi, StarWarsApi>()
            .AddSingleton<IStarWarsRepository, StarWarsRepository>()
            .AddSingleton<ISpaceTravelService, SpaceTravelService>()
            .BuildServiceProvider();

            Console.WriteLine("This console application will display the required number of resupply stops to travel between 2 planets");
            Console.WriteLine("to calculate this you only need to enter the distance between the 2 planets.");
            Console.WriteLine("");
            Console.WriteLine("Please Enter the distance between the 2 planets: ");
            string distanceBetweenPlanets = Console.ReadLine();

            var spaceTravelService = services.GetService<ISpaceTravelService>();
            var allShipsRequiredStops = spaceTravelService.GetStopsRequiredForShips(new Distance(DistanceType.MGLT, Convert.ToInt32(distanceBetweenPlanets)));

            ShowWaiting(allShipsRequiredStops);

            Console.WriteLine("Ship Name: Number Of Stops Required");

            foreach (var starShipFuelStop in allShipsRequiredStops.Result.Where(x => x.StopsRequired != null))
            {
                Console.WriteLine($"{starShipFuelStop}");
                Console.WriteLine(string.Empty);
            }

            Console.WriteLine(string.Empty);
            Console.WriteLine("The following ships we could not calculate the resupplies required");
            Console.WriteLine(string.Empty);

            foreach (var starShipFuelStop in allShipsRequiredStops.Result.Where(x => x.StopsRequired == null))
            {
                Console.WriteLine($"{starShipFuelStop}");
                Console.WriteLine(string.Empty);
            }

            Console.WriteLine("Press any key to close...");
            Console.ReadLine();

        }

        private static void ShowWaiting(Task<IList<StarWarsDomain.Model.StarShipResupplyStops>> allShipsRequiredStops)
        {
            int numberOfStops = 1;
            while (!allShipsRequiredStops.IsCompleted)
            {
                Console.WriteLine($"Please Wait {new string('.', numberOfStops)}".PadRight(30));
                Task.Delay(250).Wait();
                numberOfStops += 1;

                if (numberOfStops == 4)
                {
                    numberOfStops = 1;
                }

                Console.SetCursorPosition(0, Console.CursorTop - 1);
            }

        }
    }
}
